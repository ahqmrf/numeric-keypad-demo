package com.ahqmrf.numerickeypad

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        keypad.initInputs(this)
        keypad.registerInputView(name)
        keypad.registerInputView(amount)
        keypad.registerInputView(username)
        keypad.registerInputView(date)
    }

    override fun onBackPressed() {
        if (keypad.visibility == View.VISIBLE) {
            keypad.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }
}
