package com.ahqmrf.numerickeypad

import android.text.InputFilter
import android.text.Spanned
import android.text.TextUtils

/**
 * Created by Ariful Hoque Maruf
 * Jr Software Engineer, Brain Station-23 Ltd.
 * https://www.github.com/ahqmrf
 */
class CustomRangeInputFilter(private val minValue: Double, private val maxValue: Double) : InputFilter {

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dStart: Int, dEnd: Int): CharSequence? {
        try {
            // Remove the string out of destination that is to be replaced
            if (TextUtils.isEmpty(dest.toString())) return null
            var newVal = dest.toString().substring(0, dStart) + dest.toString().substring(dEnd, dest.toString().length)
            newVal = newVal.substring(0, dStart) + source.toString() + newVal.substring(dStart, newVal.length)
            val input = java.lang.Double.parseDouble(newVal)

            if (isInRange(minValue, maxValue, input)) {
                return null
            }
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }

        return ""
    }

    private fun isInRange(a: Double, b: Double, c: Double): Boolean {
        return if (b > a) c in a..b else c in b..a
    }
}