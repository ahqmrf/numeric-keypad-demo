package com.ahqmrf.numerickeypad

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout


/**
 * Created by Ariful Hoque Maruf
 * Jr Software Engineer, Brain Station-23 Ltd.
 * https://www.github.com/ahqmrf
 */
class NumericKeypad : LinearLayout {

    private var inputConnection: InputConnection? = null
    private var inputsInitialized = false
    private lateinit var activity : Activity
    private lateinit var root: View

    constructor(context: Context) : super(context) {
        init(context, null, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr, 0)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP) constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, defStyleRes)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        val inflater = LayoutInflater.from(context)
        val pinLayout = inflater.inflate(R.layout.keypad, this, true)

        setKeyBoardListener(pinLayout)
    }

    private fun setKeyBoardListener(view: View) {
        when (view) {
            is AppCompatImageView -> view.setOnClickListener({
                if (inputConnection != null) {
                    val selectedText = inputConnection?.getSelectedText(0) ?: ""
                    if (TextUtils.isEmpty(selectedText)) {
                        // no selection, so delete previous character
                        inputConnection?.deleteSurroundingText(1, 0)
                    } else {
                        // delete the selection
                        inputConnection?.commitText("", 1)
                    }
                }
            })
            is AppCompatTextView -> view.setOnClickListener({
                inputConnection?.commitText(view.text.toString(), 1)
            })
            is ViewGroup -> {
                val count = view.childCount
                if (count > 0) {
                    for (i in 1..count) {
                        setKeyBoardListener(view.getChildAt(i - 1))
                    }
                }
            }
        }
    }

    private fun setInputConnection(inputConnection: InputConnection?) {
        this.inputConnection = inputConnection
    }

    fun initInputs(context: Context) {
        activity = context as Activity
        root = activity.window.decorView.findViewById<View>(android.R.id.content)
        unregisterView(rootView)
        inputsInitialized = true
    }

    private fun unregisterView(view: View?) {
        when (view) {
            is EditText -> ignoreInputView(view)
            is AppCompatEditText -> ignoreInputView(view)
            is ViewGroup -> {
                val childCount = view.childCount
                for (i in 1..childCount) {
                    unregisterView(view.getChildAt(i - 1))
                }
            }
        }
    }

    fun registerInputView(view: View) {
        if (!inputsInitialized) {
            throw Exception(javaClass.name + ".initInputs() method must be called before registering input views.")
        }
        view.setOnClickListener {
            showKeypad(it)
        }

        view.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                showKeypad(v)
            }
        }
    }

    private fun showKeypad(view: View) {
        val inputView = view as EditText
        inputView.inputType = InputType.TYPE_NULL
        hideKeyboard(view)
        inputView.setRawInputType(InputType.TYPE_CLASS_TEXT)
        inputView.setTextIsSelectable(true)
        val ic = inputView.onCreateInputConnection(EditorInfo())
        setInputConnection(ic)
        if (visibility == View.GONE) visibility = View.VISIBLE
    }

    private fun ignoreInputView(view: View) {
        view.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                setInputConnection(null)
                this.visibility = View.GONE
            }
        }
    }

    private fun hideKeyboard(view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}